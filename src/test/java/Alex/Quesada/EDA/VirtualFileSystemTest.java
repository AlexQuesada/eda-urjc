package Alex.Quesada.EDA;

import Alex.Quesada.EDA.tree.usecase.VirtualFileSystem;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class VirtualFileSystemTest {

    private final String INVALID_ID_MESSAGE = "Invalid ID.";
    private String path = "C:\\Users\\aquem\\Desktop\\prueba";
    private String path2 = "C:\\Users\\aquem\\Desktop\\prueba\\1\\11";
    private VirtualFileSystem vfs;

    @BeforeEach
    void setUp() {
        vfs = new VirtualFileSystem();
        vfs.loadFileSystem(path);
    }

    @Test
    void loadFileSystem() {
        String output = vfs.getFileSystem();
        String expected = "0\tC:\\Users\\aquem\\Desktop\\prueba\\\n" +
                "1\t\t1\\\n" +
                "2\t\t\t11\\\n" +
                "3\t\t\t12.txt\n" +
                "4\t\t\t13.txt\n" +
                "5\t\t2\\\n" +
                "6\t\t\t21\\\n" +
                "7\t\t\t\t211.txt\n" +
                "8\t\t\t22.txt\n" +
                "9\t\t\t23.txt\n" +
                "10\t\t\t24.txt\n" +
                "11\t\t3\\\n" +
                "12\t\t\t31\\\n" +
                "13\t\t\t\t311.txt\n";
        assertEquals(expected, output);
    }

    @Test
    void moveFileById() {
        vfs.moveFileById(3,5);
        String output = vfs.getFileSystem();

        String expected = "0\tC:\\Users\\aquem\\Desktop\\prueba\\\n" +
                "1\t\t1\\\n" +
                "2\t\t\t11\\\n" +
                "3\t\t\t13.txt\n" +
                "4\t\t2\\\n" +
                "5\t\t\t21\\\n" +
                "6\t\t\t\t211.txt\n" +
                "7\t\t\t22.txt\n" +
                "8\t\t\t23.txt\n" +
                "9\t\t\t24.txt\n" +
                "10\t\t\t12.txt\n" +
                "11\t\t3\\\n" +
                "12\t\t\t31\\\n" +
                "13\t\t\t\t311.txt\n";
        assertEquals(expected,output);


        vfs.moveFileById(10,1);
        output = vfs.getFileSystem();
        expected = "0\tC:\\Users\\aquem\\Desktop\\prueba\\\n" +
                "1\t\t1\\\n" +
                "2\t\t\t11\\\n" +
                "3\t\t\t13.txt\n" +
                "4\t\t\t12.txt\n" +
                "5\t\t2\\\n" +
                "6\t\t\t21\\\n" +
                "7\t\t\t\t211.txt\n" +
                "8\t\t\t22.txt\n" +
                "9\t\t\t23.txt\n" +
                "10\t\t\t24.txt\n" +
                "11\t\t3\\\n" +
                "12\t\t\t31\\\n" +
                "13\t\t\t\t311.txt\n";
        assertEquals(expected, output);


        //Al mover una carpeta se mueve con el su contenido.
        vfs.moveFileById(1, 5);
        output = vfs.getFileSystem();
        expected = "0\tC:\\Users\\aquem\\Desktop\\prueba\\\n" +
                "1\t\t2\\\n" +
                "2\t\t\t21\\\n" +
                "3\t\t\t\t211.txt\n" +
                "4\t\t\t22.txt\n" +
                "5\t\t\t23.txt\n" +
                "6\t\t\t24.txt\n" +
                "7\t\t\t1\\\n" +
                "8\t\t\t\t11\\\n" +
                "9\t\t\t\t13.txt\n" +
                "10\t\t\t\t12.txt\n" +
                "11\t\t3\\\n" +
                "12\t\t\t31\\\n" +
                "13\t\t\t\t311.txt\n";
        assertEquals(expected, output);

    }

    @Test
    void moveFileById_Exceptions(){
        RuntimeException exception = assertThrows(RuntimeException.class, () -> vfs.moveFileById(3, 4));
        assertEquals("Target can't be a file.", exception.getMessage());
    }

    @Test
    void removeFileById() {
        vfs.removeFileById(4);
        String output = vfs.getFileSystem();
        String expected="0\tC:\\Users\\aquem\\Desktop\\prueba\\\n" +
                "1\t\t1\\\n" +
                "2\t\t\t11\\\n" +
                "3\t\t\t12.txt\n" +
                "4\t\t2\\\n" +
                "5\t\t\t21\\\n" +
                "6\t\t\t\t211.txt\n" +
                "7\t\t\t22.txt\n" +
                "8\t\t\t23.txt\n" +
                "9\t\t\t24.txt\n" +
                "10\t\t3\\\n" +
                "11\t\t\t31\\\n" +
                "12\t\t\t\t311.txt\n";
        assertEquals(expected, output);
    }

    @Test
    void findBySubstring() {
//        Iterable<String> iterable = vfs.findBySubstring(0,"A");
//        String output = iterable.toString();
//        String expected = "[1\tSubdirectorioA, 2\tArchivoA.ext, 3\tArchivoB.ext, 6\tArchivoC.ext, 10\tArchivoD.ext]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySubstring(0,".ext");
//        output = iterable.toString();
//        expected = "[2\tArchivoA.ext, 3\tArchivoB.ext, 6\tArchivoC.ext, 10\tArchivoD.ext]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySubstring(8,"");
//        output = iterable.toString();
//        expected = "[8\tSubdirectorioE, 9\tSubdirectorioF, 10\tArchivoD.ext]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySubstring(8,"F");
//        output = iterable.toString();
//        expected = "[9\tSubdirectorioF]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySubstring(4,"Z");
//        output = iterable.toString();
//        expected = "[]";
//        assertEquals(expected,output);

    }

    @Test
    void findBySize() {
//        Iterable<String> iterable = vfs.findBySize(0, 1, 19);
//        String output = iterable.toString();
//        String expected = "[3\tArchivoB.ext, 6\tArchivoC.ext]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySize(0, 0, 1000);
//        output = iterable.toString();
//        expected = "[2\tArchivoA.ext, 3\tArchivoB.ext, 6\tArchivoC.ext, 10\tArchivoD.ext]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySize(7, 0, 1000);
//        output = iterable.toString();
//        expected = "[]";
//        assertEquals(expected,output);
//
//        iterable = vfs.findBySize(0, 1, 1);
//        output = iterable.toString();
//        expected = "[3\tArchivoB.ext]";
//        assertEquals(expected,output);
    }

    @Test
    void getFileVirtualPath() {
//        String output, expected;
//        output = vfs.getFileVirtualPath(0);
//        expected = "vfs:/DirectorioRaiz";
//        assertEquals(expected, output);
//
//        output = vfs.getFileVirtualPath(6);
//        assertEquals("vfs:/DirectorioRaiz/SubdirectorioB/SubdirectorioC/ArchivoC.ext",output);
//
//        vfs.moveFileById(8,5);
//        output = vfs.getFileVirtualPath(10);
//        expected = "vfs:/DirectorioRaiz/SubdirectorioB/SubdirectorioC/SubdirectorioE/SubdirectorioF/ArchivoD.ext";
//        assertEquals(expected,output);
//
//        vfs.moveFileById(9,1);
//        output = vfs.getFileVirtualPath(9);
//        expected = "vfs:/DirectorioRaiz/SubdirectorioA/SubdirectorioF";
//        assertEquals(expected, output);
    }

    @Test
    void getFilePath() {
//        String output, expected;
//        output = vfs.getFilePath(0);
//        expected = "./Resources/practica2/DirectorioRaiz";
//        assertEquals(expected, output);
//
//        output = vfs.getFilePath(3);
//        expected = "./Resources/practica2/DirectorioRaiz/SubdirectorioA/ArchivoB.ext";
//        assertEquals(expected, output);
//
//        vfs.loadFileSystem(path2);
//        output = vfs.getFilePath(0);
//        expected = "./Resources/practica2/DirectorioRaiz/SubdirectorioA";
//        assertEquals(expected, output);
    }


}
