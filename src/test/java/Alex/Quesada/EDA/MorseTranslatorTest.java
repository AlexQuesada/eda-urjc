package Alex.Quesada.EDA;

import Alex.Quesada.EDA.tree.usecase.MorseTranslator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MorseTranslatorTest {

    MorseTranslator translatorSample;

    @BeforeEach
    void setUp() {
        char[] charsetSample = {'a', 'h', 'r', 'u', 'l',
                'n', 'd', 'o', 'm', 'e',
                'w'};
        String[] codesSample = {".", "..", "...", "..-", ".-"
                , ".-.", ".--", "-", "-.", "--."
                , "---"};

        translatorSample = new MorseTranslator(charsetSample, codesSample);

    }


    @Test
    void decode() {
        String input, expected, output;
        input = ". ";
        expected = "a";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = ".";
        expected = "a";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = ". .";
        expected = "aa";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = "..";
        expected = "h";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = "... ";
        expected = "r ";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = ".. " + "- " + ".- " + ". " + " " + "-." + "..-" + ".-." + ".--" + "-";
        expected = "hola mundo";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = ".. --..- .- -  ---- ....- .--";
        expected = "hello world";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

        input = "-.- .----.....-.-";
        expected = "moderno";
        output = translatorSample.decode(input);
        assertEquals(expected, output);

    }

    @Test
    void encode() {
        String input, expected, output;
        input = "a";
        expected = ". ";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "a ";
        expected = ".  ";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "a a";
        expected = ".  . ";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "h";
        expected = ".. ";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "u";
        expected = "..-";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "hola mundo";
        expected = ".. " + "- " + ".- " + ". " + " " + "-." + "..-" + ".-." + ".--" + "- ";
        output = translatorSample.encode(input);
        assertEquals(expected, output);

        input = "hello world";
        expected = ".. --..- .- -  ---- ....- .--";
        output = translatorSample.encode(input);
        assertEquals(expected, output);


    }
}