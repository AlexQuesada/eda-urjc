package Alex.Quesada.EDA;

public interface Position<E> {

    /**
     * Checks the element stored at this position.
     *
     * @return the element stored in the given position
     */
    E getElement();

}