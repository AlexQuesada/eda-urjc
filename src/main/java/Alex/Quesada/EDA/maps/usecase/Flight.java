package Alex.Quesada.EDA.maps.usecase;

import Alex.Quesada.EDA.maps.HashTableMapLP;

import java.time.LocalDate;
import java.util.Comparator;

public class Flight implements Comparable {

    private static class Date {
        public int year;
        public int month;
        public int day;

        public Date(int year, int month, int day) {
            this.year = year;
            this.month = month;
            this.day = day;
        }

        public int getYear() {
            return year;
        }

        public void setYear(int year) {
            this.year = year;
        }

        public int getMonth() {
            return month;
        }

        public void setMonth(int month) {
            this.month = month;
        }

        public int getDay() {
            return day;
        }

        public void setDay(int day) {
            this.day = day;
        }
    }

    private static class Time {
        public int hours;
        public int minutes;

        public Time(int hours, int minutes) {
            this.hours = hours;
            this.minutes = minutes;
        }

        public int getHours() {
            return hours;
        }

        public void setHours(int hours) {
            this.hours = hours;
        }

        public int getMinutes() {
            return minutes;
        }

        public void setMinutes(int minutes) {
            this.minutes = minutes;
        }
    }

    Id id;
    String origin, destination;
    int capacity, delay;
    Time time;
    HashTableMapLP<String, String> properties;

    public Flight(){
        id = new Id();
    }

    public Flight(String company, int flightCode, int year, int month, int day) {
        this.id = new Id(company, flightCode, year, month, day);
    }

    public Flight(Id id) {
        this.id = id;
    }

    public Id getId() {
        return id;
    }

    public void setId(Id id) {
        this.id = id;
    }

    public String getCompany() {
        return id.company;
    }

    public void setCompany(String company) {
        id.company = company;
    }

    public String getOrigin() {
        return origin;
    }

    public void setOrigin(String origin) {
        this.origin = origin;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFlightCode() {
        return id.flightCode;
    }

    public void setFlightCode(int flightCode) {
        id.flightCode = flightCode;
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public int getDelay() {
        return delay;
    }

    public void setDelay(int delay) {
        this.delay = delay;
    }

    public void setDate (int year, int month, int day){
        id.setYear(year);
        id.setMonth(month);
        id.setDay(day);
    }

    public int getYear(){
        return id.getYear();
    }

    public int getMonth(){
        return id.getMonth();
    }

    public int getDay(){
        return id.getDay();
    }

    public void setTime(int hours, int minutes){
        time.setHours(hours);
        time.setMinutes(minutes);
    }

    public int getHours(){
        return time.getHours();
    }

    public int getMinutes(){
        return time.getMinutes();
    }

    public void setProperty(String attribute, String value){
        properties.put(attribute, value);
    }

    public String getProperty(String attribute){
        return properties.get(attribute);
    }

    public void setAllProperties(HashTableMapLP<String, String> prop){
        this.properties = prop;
    }

    public HashTableMapLP<String,String> getAllProperties(){
        return properties;
    }

    public Iterable<String> getAllAttributes() {
        throw new RuntimeException("Not yet implemented.");
    }

    @Override
    public String toString() {
        return getDay() + "-" + getMonth() + "-" + getYear() + "\t" +
                getCompany()+getFlightCode() + "\t" + destination;
    }

    @Override
    public int compareTo(Object o) {
        Flight f = (Flight) o;
        LocalDate date1 = LocalDate.of(this.getYear(), this.getMonth(), this.getDay());
        LocalDate date2 = LocalDate.of(f.getYear(), f.getMonth(), f.getDay());
        if (date1.equals(date2)){
            if (this.getFlightCode() > f.getFlightCode()){
                return -1;
            } else {
                return 1;
            }
        } else if (date1.isBefore(date2))
            return 1;
        else
            return -1;
    }



}