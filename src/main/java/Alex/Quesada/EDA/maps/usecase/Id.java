package Alex.Quesada.EDA.maps.usecase;

import java.time.LocalDate;
import java.util.Date;

public class Id {
    public String company;
    public int flightCode;
    public LocalDate date;
//    public Date date;

    public Id () {
        date = LocalDate.now();
    }

    public Id(String company, int flightCode, int year, int month, int day) {
        this.company = company;
        this.flightCode = flightCode;
        date = LocalDate.of(year, month, day);
//        this.date = new Date(year, month, day);
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public int getFlightCode() {
        return flightCode;
    }

    public void setFlightCode(int flightCode) {
        this.flightCode = flightCode;
    }

    public int getYear() {
        return date.getYear();
    }

    public void setYear(int year) {
        date = date.withYear(year);
    }

    public int getMonth() {
        return date.getMonthValue();
    }

    public void setMonth(int month) {
        date =  date.withMonth(month);
    }

    public int getDay() {
        return date.getDayOfMonth();
    }

    public void setDay(int day) {
        date = date.withDayOfMonth(day);
    }

    @Override
    public String toString() {
        return "Company: " + company + "\n" +
                "FlightCode: " + flightCode + "\n" +
                "Date: " + date.toString() ;
    }

    @Override
    public boolean equals(Object o){
        //todo: this
        return false;
    }
}
