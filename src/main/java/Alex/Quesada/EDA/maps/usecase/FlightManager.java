package Alex.Quesada.EDA.maps.usecase;

import Alex.Quesada.EDA.maps.HashTableMapLP;

import java.util.Date;
import java.util.LinkedList;

public class FlightManager {

    private class FlightInfo {
        public Flight flight;
        public LinkedList<Passenger> passengers;

        public FlightInfo(Flight flight) {
            this.flight = flight;
            passengers = new LinkedList<>();
        }

        public FlightInfo(Id id) {
            flight = new Flight(id);
            passengers = new LinkedList<>();
        }

        public FlightInfo(String company, int flightCode, int year, int month, int day){
            flight = new Flight(company, flightCode, year, month, day);
            passengers = new LinkedList<>();
        }
    }

    private HashTableMapLP<Id, FlightInfo> flights;

    public FlightManager (){
        flights = new HashTableMapLP<>();
    }

    public Flight addFlight(String company, int flightCode, int year, int month, int day) {
        Id id = new Id(company, flightCode, year, month, day);
        Flight flight = new Flight(id);
        FlightInfo flightInfo = new FlightInfo(id);
        if (flights.put(id, flightInfo) != null){
            throw new RuntimeException("There was already a flight with the same id: " + id.toString());
        }
        return flight;
    }

    public Flight addFlight(String company, int flightCode, int year, int month, int day, String destination) {
        Id id = new Id(company, flightCode, year, month, day);
        Flight flight = new Flight(id);
        FlightInfo flightInfo = new FlightInfo(id);
        flightInfo.flight.setDestination(destination);
        if (flights.put(id, flightInfo) != null){
            throw new RuntimeException("There was already a flight with the same id: " + id.toString());
        }
        return flight;
    }

    public Flight getFlight(String company, int flightCode, int year, int month, int day) {
        Id id = new Id (company, flightCode, year, month, day);
        FlightInfo flightInfo = flights.get(id);
        if (flightInfo == null)
            throw new RuntimeException("getFlight method failed");
        Flight flight = flightInfo.flight;
        return new Flight(flight.getId());
    }

    public void updateFlight(String company, int flightCode, int year, int month, int day, Flight updatedFlightInfo) {
        Id id = new Id (company, flightCode, year, month, day);
        FlightInfo flightInfo = flights.get(id);
        if (flightInfo == null) {
            throw new RuntimeException("The flight with doesn't exist");
        }
        flightInfo.flight.setOrigin(updatedFlightInfo.getOrigin());
        flightInfo.flight.setDestination(updatedFlightInfo.getDestination());
        flightInfo.flight.setCapacity(updatedFlightInfo.getCapacity());
        flightInfo.flight.setDelay(updatedFlightInfo.getDelay());
        flightInfo.flight.setTime(updatedFlightInfo.getHours(), updatedFlightInfo.getMinutes());
        flightInfo.flight.setAllProperties(updatedFlightInfo.getAllProperties());
        if (!updatedFlightInfo.getId().equals(id)) {
            flightInfo.flight.setId(updatedFlightInfo.getId());
            flights.remove(id);
            flights.put(flightInfo.flight.getId(), flightInfo);
        }
    }

    public void addPassenger(String dni, String name, String surname, Flight flight) {
        Passenger passenger = new Passenger(dni, name, surname);
        FlightInfo flightInfo = flights.get(flight.getId());
        if (flightInfo == null)
            throw new RuntimeException("The flight doen't exist");
        if (flightInfo.flight.getCapacity() == 0)
            throw new RuntimeException("No capacity in the flight");

        boolean alreadyBookedIn = false;
        for (Passenger p: flightInfo.passengers) {
            if (p.getDNI().equals(dni)) {
                alreadyBookedIn = true;
                p.setName(name);
                p.setSurname(surname);
                break;
            }
        }
        if (!alreadyBookedIn){
            flightInfo.passengers.add(passenger);
            flightInfo.flight.setCapacity(flightInfo.flight.getCapacity() - 1);
        }
    }

    public Iterable<Passenger> getPassengers(String company, int flightCode, int year, int month, int day) {
        Id id = new Id(company, flightCode, year, month, day);
        FlightInfo flightInfo = flights.get(id);
        if (flightInfo == null)
            throw new RuntimeException();
        return flightInfo.passengers;
    }

    public Iterable<Flight> flightsByDate(int year, int month, int day) {
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(FlightInfo f: flights.values())
            if (f.flight.getYear() == year && f.flight.getMonth() == month && f.flight.getDay() == day)
                flightsList.add(f.flight);

        return flightsList;
    }

    public Iterable<Flight> getFlightsByPassenger(Passenger passenger) {
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(FlightInfo f: flights.values())
            for (Passenger p : f.passengers)
                if (p.equals(passenger))
                    flightsList.add(f.flight);

        return flightsList;
    }

    public Iterable<Flight> getFlightsByDestination(String destination, int year, int month, int day) {
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(Flight f: flightsByDate(year, month, day))
            if (f.destination.startsWith(destination))
                flightsList.add(f);
        return flightsList;

    }

    public Iterable<Flight> getFlightsByDestination(String destination) {
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(FlightInfo f: flights.values())
            if (f.flight.destination.startsWith(destination))
                flightsList.add(f.flight);
        return flightsList;
    }

    public Iterable<Flight> getFlightsByCompany(String company) {
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(FlightInfo f: flights.values())
            if (f.flight.getCompany().startsWith(company))
                flightsList.add(f.flight);
        return flightsList;
    }

    public Iterable<Flight> getAllFlights(){
        LinkedList<Flight> flightsList = new LinkedList<>();
        for(FlightInfo f: flights.values())
            flightsList.add(f.flight);
        return flightsList;
    }















}
