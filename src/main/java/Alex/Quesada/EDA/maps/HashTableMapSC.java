package Alex.Quesada.EDA.maps;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

public class HashTableMapSC<K, V> implements Map<K, V> {

    private class HashEntries<T,U> {
        public LinkedList<HashEntry<T,U>> collisions;

        public HashEntries(T t, U u){
            collisions = new LinkedList<>();
            add(t, u);
        }

        public void add(T t, U u){
            HashEntry<T,U> entry = new HashEntry<>(t, u);
            collisions.add(entry);
        }
    }

    private class HashEntry<T, U> implements Entry<T, U> {

        protected T key;
        protected U value;

        public HashEntry(T k, U v) {
            key = k;
            value = v;
        }

        @Override
        public U getValue() {
            return value;
        }

        @Override
        public T getKey(){
            return key;
        }
    }

    private class HashTableMapIterator<T, U> implements Iterator<Entry<T, U>> {

        public HashTableMapIterator(List<HashEntry<T, U>>[] map, int numElems) {
            throw new RuntimeException("Not yet implemented.");
        }


        @Override
        public boolean hasNext() {
            throw new RuntimeException("Not yet implemented.");
        }

        @Override
        public Entry<T, U> next() {
            throw new RuntimeException("Not yet implemented.");
        }

    }

    private int n; // number of entries in the dictionary
    private int prime, capacity; // prime factor and capacity of bucket array
    private long scale, shift; // the shift and scaling factors
    private HashEntries<K, V>[] bucket;// bucket array
    private final double LOAD_FACTOR = 0.75;

    public HashTableMapSC() {
        this(109345121, 1000); // reusing the constructor HashTableMap(int p, int cap)
    }

    public HashTableMapSC(int cap) {
        this(109345121, cap); // reusing the constructor HashTableMap(int p, int cap)
    }

    public HashTableMapSC (int p, int cap){
        n = 0;
        prime = p;
        capacity = cap;
        bucket = (HashEntries<K,V>[]) new HashEntries[capacity];
        Random rand = new Random();
        this.scale = rand.nextInt(prime - 1) + 1;
        this.shift = rand.nextInt(prime);
    }

    @Override
    public int size(){
        return n;
    }

    @Override
    public boolean isEmpty() {
        return n == 0;
    }

    @Override
    public V get(K key) {
        checkKey(key);

        V value = null;
        int hashKey = hashValue(key);

        if (bucket[hashKey] != null){
            for (HashEntry<K,V> entry: bucket[hashKey].collisions){
                if (entry.key.equals(key))
                    value = entry.value;
            }
        }

        return value;
    }

    @Override
    public V put(K key, V value) {
        checkKey(key);
        V v = null;

        if ((double) (n + 1) / capacity >= LOAD_FACTOR){
            bucket = rehash();
        }

        int hashKey = hashValue(key);
        if (bucket[hashKey] == null){
            bucket[hashKey] = new HashEntries<>(key, value);
            n ++;
        } else {
            for (HashEntry<K,V> entry: bucket[hashKey].collisions){
                if (entry.key.equals(key)){
                    v = entry.value;
                    entry.value = value;
                }
            }
            if (v == null){
                n ++;
                bucket[hashKey].add(key, value);
            }
        }

        return v;
    }

    @Override
    public V remove(K key) {
        checkKey(key);

        V value = null;
        int hashKey = hashValue(key);
        HashEntry<K,V> h = null;

        if (bucket[hashKey] != null){
            for (HashEntry<K,V> entry: bucket[hashKey].collisions){
                if (entry.key.equals(key)){
                    value = entry.value;
                    n --;
                    h = entry;
                }
            }
        }

        if (h != null)
            bucket[hashKey].collisions.remove(h);


        return value;
    }

    @Override
    public Iterable<K> keys() {
        LinkedList<K> keys = new LinkedList<>();

        for (HashEntries<K,V> entries: bucket){
            if (entries != null){
                for (HashEntry<K,V> entry: entries.collisions){
                    keys.add (entry.key);
                }
            }
        }

        return keys;
    }

    @Override
    public Iterable<V> values() {
        LinkedList<V> values = new LinkedList<>();

        for (HashEntries<K,V> entries: bucket){
            if (entries != null){
                for (HashEntry<K,V> entry: entries.collisions){
                    values.add(entry.value);
                }
            }
        }

        return values;
    }

    @Override
    public Iterable<Entry<K, V>> entries() {
        LinkedList<Entry<K,V>> e = new LinkedList<>();

        for (HashEntries<K,V> entries: bucket){
            if (entries != null){
                e.addAll(entries.collisions);
            }
        }

        return e;
    }

    @Override
    public Iterator<Entry<K, V>> iterator() {
        return null;
    }

    /**
     * Determines whether a key is valid.
     *
     * @param k Key
     */
    private void checkKey(K k) {
        if (k == null) {
            throw new IllegalStateException("Invalid key: null.");
        }
    }

    /**
     * Increase/reduce the size of the hash table and rehashes all the entries.
     */
    protected HashEntries<K, V>[] rehash() {
        capacity = capacity*2;
        HashEntries<K,V>[] newBucket = (HashEntries<K,V>[]) new HashEntries[capacity];

        for (HashEntries<K,V> entries: bucket){
            if (entries != null){
                for (HashEntry<K,V> entry: entries.collisions){
                    int hashKey = hashValue(entry.key);
                    if (newBucket[hashKey] == null){
                        newBucket[hashKey] = new HashEntries<K,V>(entry.key, entry.value);
                    }
                    else {
                        newBucket[hashKey].add(entry.key, entry.value);
                    }
                }
            }
        }
        return newBucket;
    }

    public int hashValue(K key){
        return (int) ((Math.abs(key.hashCode() * scale + shift) % prime) % capacity);
    }
}
