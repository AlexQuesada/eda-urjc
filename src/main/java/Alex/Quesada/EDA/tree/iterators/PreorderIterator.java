package Alex.Quesada.EDA.tree.iterators;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.Tree;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.function.Predicate;

/**
 * Práctica 2, ejercicio 3
 */
public class PreorderIterator<E> implements Iterator<Position<E>> {

    private Deque<Position<E>> stack;
    private Tree<E> tree;
    private Predicate<Position<E>> predicate;


    public PreorderIterator(Tree<E> tree) {
        this.tree = tree;
        stack = new LinkedList<>();
        stack.add(tree.root());
        predicate = p -> true;
    }

    public PreorderIterator(Tree<E> tree, Position<E> start) {
        this.tree = tree;
        stack = new LinkedList<>();
        stack.add(start);
        predicate = p -> true;
    }

    public PreorderIterator(Tree<E> tree, Position<E> start, Predicate<Position<E>> p) {
        this.tree = tree;
        stack = new LinkedList<>();
        predicate = p;
        stack.add(start);
    }


    @Override
    public boolean hasNext() {
        return !this.stack.isEmpty();
    }

    @Override
    public Position<E> next() {
        Position<E> next = stack.pollFirst();

        if (next == null)
            return null;

        Deque<Position<E>> children = new LinkedList<>();

        for (Position<E> child : tree.children(next))
            children.addFirst(child);

        for (Position<E> child : children)
            stack.addFirst(child);

        if (!predicate.test(next)){
            next = next();
        }
        return next;
    }
}