package Alex.Quesada.EDA.tree.iterators;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.Tree;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class BFSIterator<E> implements Iterator<Position<E>> {

    private Deque<Position<E>> queue;
    private Tree<E> tree;

    public BFSIterator(Tree<E> t, Position<E> p) {
        tree = t;
        queue = new LinkedList<>();
        queue.add(p);
    }

    public BFSIterator(Tree<E> t){
        tree = t;
        queue = new LinkedList<>();
        queue.add(t.root());
    }

    @Override
    public boolean hasNext() {
        return !this.queue.isEmpty();
    }

    @Override
    public Position<E> next() {
        Position<E> pos = queue.pollFirst();
        for (Position<E> p: tree.children(pos))
            queue.addLast(p);
        return pos;
    }
}
