package Alex.Quesada.EDA.tree.iterators;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.binaryTree.BinaryTree;

import java.util.Deque;
import java.util.Iterator;
import java.util.LinkedList;

public class InOrderBinaryTreeIterator<E> implements Iterator<Position<E>> {

    private Deque<Position<E>> nodeStack;
    private BinaryTree<E> tree;

    public InOrderBinaryTreeIterator(BinaryTree<E> t) {
        this.tree = t;
        nodeStack = new LinkedList<>();
        if (!tree.isEmpty())
            goToLastInLeft(tree.root());
    }

    public InOrderBinaryTreeIterator(BinaryTree<E> t, Position<E> p) {
        this.tree = t;
        nodeStack = new LinkedList<>();
        goToLastInLeft(p);
    }

    private void goToLastInLeft(Position<E> node){
        nodeStack.addFirst(node);
        while (tree.hasLeft(node)){
            nodeStack.addFirst(tree.left(node));
            node = tree.left(node);
        }
    }

    @Override
    public boolean hasNext() {
        return !nodeStack.isEmpty();
    }

    @Override
    public Position<E> next() {
        Position<E> aux = nodeStack.pollFirst();
        if (tree.hasRight(aux)){
            goToLastInLeft(tree.right(aux));
        }
        return aux;
    }
}
