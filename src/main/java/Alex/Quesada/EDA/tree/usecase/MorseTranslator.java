package Alex.Quesada.EDA.tree.usecase;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.binaryTree.BinaryTree;
import Alex.Quesada.EDA.tree.binaryTree.LinkedBinaryTree;

import java.util.Deque;
import java.util.LinkedList;

public class MorseTranslator {

    private BinaryTree<Character> tree;

    /**
     * Generates a new MorseTranslator instance given two arrays:
     * one with the character set and another with their respective
     * morse code.
     *
     * @param charset
     * @param codes
     */
    public MorseTranslator(char[] charset, String[] codes) {
        tree = new LinkedBinaryTree<>();
        tree.addRoot(null);
        for (int i = 0; i < charset.length; i++){
            Position<Character> aux = tree.root();
            char[] code = codes[i].toCharArray();
            for (char c : code) {
                if (c == '.') {
                    if (tree.hasLeft(aux))
                        aux = tree.left(aux);
                    else {
                        aux = tree.insertLeft(aux, null);
                    }
                } else if (c == '-') {
                    if (tree.hasRight(aux))
                        aux = tree.right(aux);
                    else {
                        aux = tree.insertRight(aux, null);
                    }
                } else
                    throw new RuntimeException("invalid code");
            }
            tree.replace(aux, charset[i]);
        }
    }

    /**
     * Decodes a String with a message in morse code and returns
     * another String in plaintext. The input String may contain
     * the characters: ' ', '-' '.'.
     *
     * @param morseMessage
     * @return a plain text translation of the morse code
     */
    public String decode(String morseMessage) {
        char[] msg = morseMessage.toCharArray();
        String result = "";
        Position<Character> aux = tree.root();
        for (char c : msg) {
            if (c == '.') {
                if (tree.hasLeft(aux)) {
                    aux = tree.left(aux);
                    if (!tree.isInternal(aux)) {
                        result = result.concat(aux.getElement().toString());
                        aux = tree.root(); //restart the search
                    }
                } else {
                    throw new RuntimeException(morseMessage + " is not contemplated with the codification stablished");
                }
            } else if (c == '-') {
                if (tree.hasRight(aux)) {
                    aux = tree.right(aux);
                    if (!tree.isInternal(aux)) {
                        result = result.concat(aux.getElement().toString());
                        aux = tree.root(); //restart the search
                    }
                } else {
                    throw new RuntimeException(morseMessage + " is not contemplated with the codification stablished");
                }
            } else { // if (c == ' ')
                if (tree.isInternal(aux)) { //there's ambiguity
                    if (aux.getElement() != null) {
                        result = result.concat(aux.getElement().toString());
                        aux = tree.root(); //restart the search
                    } else {
                        if (aux == tree.root())
                            result = result.concat(" ");
                        else
                            throw new RuntimeException(morseMessage + " is not contemplated with the codification stablished");
                    }
                } else { //no ambiguity
                    result = result.concat(aux.getElement().toString() + " ");
                }
            }
        }
        if (msg[msg.length-1] != ' ' && aux != tree.root()){ //last char was ambiguous
            result = result.concat(aux.getElement().toString());
        }
        return result;
    }


    /**
     * Receives a String with a message in plaintext. This message
     * may contain any character in the charset.
     *
     * @param plainText
     * @return a morse code message
     */
    public String encode(String plainText) {
        char[] msg = plainText.toCharArray();
        String result = "";
        //Position<Character> aux = tree.root();
        for (char c: msg){
            if (c == ' '){
                result = result.concat(" ");
            } else {
                for (Position<Character> aux : tree) {
                    if (aux != tree.root() && aux.getElement()!=null){
                        if (aux.getElement() == c){ //todo: if aux.getElement() == null falla
                            Deque<Character> stack = new LinkedList<>();
                            if (tree.isInternal(aux))
                                stack.addFirst(' ');
                            while (aux != tree.root()){
                                Position<Character> parent = tree.parent(aux);
                                if (tree.hasLeft(parent)){
                                    if (tree.left(parent) == aux){
                                        stack.addFirst('.');
                                    }
                                } if (tree.hasRight(parent)){
                                    if (tree.right(parent) == aux){
                                        stack.addFirst('-');
                                    }
                                }
                                aux = parent;
                            }
                            for (char x: stack){
                                result = result.concat(Character.toString(x));
                            }
                            break;
                        }
                    }
                }
            }
        }
        return result;
    }


}