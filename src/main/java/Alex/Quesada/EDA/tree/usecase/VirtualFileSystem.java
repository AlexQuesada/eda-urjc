package Alex.Quesada.EDA.tree.usecase;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.iterators.PreorderIterator;
import Alex.Quesada.EDA.tree.naryTree.LCRSTree;

import java.io.File;
import java.util.Collection;
import java.util.LinkedList;

public class VirtualFileSystem {

    private class VirtualFile{
        private File file;
        private String name;
        private int level;

        public VirtualFile(File file, String name, int level) {
            this.file = file;
            this.name = name;
            this.level = level;
        }

        public File getFile() {
            return file;
        }

        public void setFile(File file) {
            this.file = file;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getLevel() {
            return level;
        }

        public void setLevel(int level) {
            this.level = level;
        }
    }

    private LCRSTree<VirtualFile> tree;
    private LinkedList<Position<VirtualFile>>positionList;

    public VirtualFileSystem() {
        tree = new LCRSTree<>();
        positionList = new LinkedList<>();
    }

    public void loadFileSystem(String path) {
        VirtualFile vf = new VirtualFile(new File(path), path + "\\", 0);
        tree.addRoot(vf);
        positionList.add(tree.root());
        fileTree(new File(path), tree.root(), 1);
    }

    public String getFileSystem() {
        StringBuilder result = new StringBuilder();
        int i = 0;
        PreorderIterator<VirtualFile> it = new PreorderIterator<>(tree);
        while (it.hasNext()){
            Position<VirtualFile> pos = it.next();
            StringBuilder tabs = new StringBuilder();
            for (int k = 0; k<pos.getElement().getLevel(); k++)
                tabs.append("\t");
            result.append(i).append("\t").append(tabs).append(pos.getElement().getName()).append("\n");
            i++;
        }
        return result.toString();
    }

    public void moveFileById(int i, int i1) throws RuntimeException {
        if (!positionList.get(i1).getElement().getFile().isDirectory()) {
            throw new RuntimeException("Target can't be a file.");
        }
        int levelI2 = positionList.get(i1).getElement().getLevel() + 1;
        if (!positionList.get(i).getElement().getFile().isDirectory()) {
            positionList.get(i).getElement().setLevel(levelI2);
        } else {
            PreorderIterator<VirtualFile> it = new PreorderIterator<>(tree, positionList.get(i));
            while (it.hasNext()){
                Position<VirtualFile> pos = it.next();
                if (pos == positionList.get(i))
                    pos.getElement().setLevel(levelI2);
                else
                    pos.getElement().setLevel(levelI2+1);
            }
        }
        tree.moveSubtree(positionList.get(i), positionList.get(i1));
        int j = 0;
        PreorderIterator<VirtualFile> it = new PreorderIterator<>(tree);
        while(it.hasNext()){
            Position<VirtualFile> pos = it.next();
            positionList.set(j, pos);
            j++;
        }
    }

    public void removeFileById(int i) {
        Position<VirtualFile> nodo = positionList.get(i);
        tree.remove(nodo);
        positionList.remove(i);
    }

    public Iterable<String> findBySubstring(int i, String a) {
        Collection<String> aux = new LinkedList<>();
        PreorderIterator<VirtualFile> it = new PreorderIterator<>(tree, positionList.get(i));
        while (it.hasNext()) {
            String name = it.next().getElement().getName();
            if (name.contains(a))
                aux.add(name);
        }
        return aux;
    }

    public Iterable<String> findBySize(int i, int i1, int i2) {
        Collection<String> aux = new LinkedList<>();
        PreorderIterator<VirtualFile> it = new PreorderIterator<>(tree, positionList.get(i));
        while (it.hasNext()) {
            Position<VirtualFile> next = it.next();
            String name = next.getElement().getName();
            if (next.getElement().getFile().length() >= i1 && next.getElement().getFile().length() <= i2)
                aux.add(name);
        }
        return aux;
    }

    public String getFileVirtualPath(int i) {
        Position<VirtualFile> child = positionList.get(i);
        String path = child.getElement().getFile().getName();
        Position<VirtualFile> aux = tree.parent(child);
        while (aux != tree.root()){
            String parentName = aux.getElement().getName();
            path = parentName.concat(path);
            aux = tree.parent(aux);
        }
        String root = tree.root().getElement().getName();
        path = root.concat(path);
        return path;
    }

    public String getFilePath(int i) {
        return positionList.get(i).getElement().getFile().getPath();
    }

    private void fileTree(File file, Position<VirtualFile> node, int level){
        File[] listFiles = file.listFiles();

        if (listFiles != null) {
            for (File value : listFiles) {
                if (value.isDirectory()) {
                    VirtualFile vf = new VirtualFile(value, value.getName() + "\\", level);
                    Position<VirtualFile> newDirectory = tree.add(vf, node);
                    positionList.add(newDirectory);
                    fileTree(value, newDirectory, level+1);
                } else {
                    VirtualFile vf = new VirtualFile(value, value.getName(), level);
                    Position<VirtualFile> newFile = tree.add(vf , node);
                    positionList.add(newFile);
                }
            }
        }
    }

    public static void main(String[] args){
        VirtualFileSystem vfs = new VirtualFileSystem();
        vfs.loadFileSystem("C:\\Users\\aquem\\Desktop\\prueba");
        System.out.println(vfs.getFileSystem());
//        vfs.moveFileById(1, 5);
        vfs.removeFileById(4);
        System.out.println(vfs.getFileSystem());
//        System.out.println(vfs.getFileVirtualPath(13));
//        System.out.println(vfs.getFilePath(13));
        System.out.println(vfs.findBySubstring(0,"txt"));
    }
}
