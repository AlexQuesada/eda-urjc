package Alex.Quesada.EDA.tree.binaryTree;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.iterators.InOrderBinaryTreeIterator;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LinkedBinaryTree<E> implements BinaryTree<E> {

    protected class BTNode<T> implements Position<T> {

        private T element;
        private BTNode<T> parent, left, right;
        private BinaryTree<T> myTree;

        public BTNode(T element, BTNode<T> parent, BTNode<T> left, BTNode<T> right, BinaryTree<T> myTree) {
            this.element = element;
            this.parent = parent;
            this.left = left;
            this.right = right;
            this.myTree = myTree;
        }

        public void setElement(T element) {
            this.element = element;
        }

        public BTNode<T> getParent() {
            return parent;
        }

        public void setParent(BTNode<T> parent) {
            this.parent = parent;
        }

        public BTNode<T> getLeft() {
            return left;
        }

        public void setLeft(BTNode<T> left) {
            this.left = left;
        }

        public BTNode<T> getRight() {
            return right;
        }

        public void setRight(BTNode<T> right) {
            this.right = right;
        }

        public BinaryTree<T> getMyTree() {
            return myTree;
        }

        public void setMyTree(BinaryTree<T> myTree) {
            this.myTree = myTree;
        }

        @Override
        public T getElement() {
            return element;
        }
    }

    private BTNode<E> root;

    public LinkedBinaryTree() {
        root = null;
    }

    @Override
    public int size() {
        int size = 0;
        for (Position<E> p : this) {
            size++;
        }
        return size;
    }

    @Override
    public boolean isEmpty() {
        return root == null;
    }

    @Override
    public Position<E> root() throws RuntimeException {
        if (isEmpty())
            throw new RuntimeException("The tree is empty");
        return root;
    }

    @Override
    public Position<E> parent(Position<E> p) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (node.parent == null)
            throw new RuntimeException("No parent");
        else
            return node.getParent();
    }

    @Override
    public Position<E> addRoot(E e) throws RuntimeException {
        if (!isEmpty())
            throw new RuntimeException("Tree already has a root");

        BTNode<E> node = new BTNode<>(e, null, null, null, this);
        this.root = node;
        return node;
    }

    @Override
    public Iterable<? extends Position<E>> children(Position<E> p){
        BTNode<E> node = checkPosition(p);
        List<Position<E>> children = new LinkedList<>();
        if (node.getLeft() != null)
            children.add(node.getLeft());
        if (node.getRight() != null)
            children.add(node.getRight());
        return children;
    }

    @Override
    public boolean isInternal(Position<E> v) {
        return !isLeaf(v);
    }

    @Override
    public boolean isLeaf(Position<E> p){
        BTNode<E> node = checkPosition(p);
        return node.getLeft() == null && node.getRight() == null;
    }

    @Override
    public boolean isRoot(Position<E> p){
        BTNode<E> node = checkPosition(p);
        return this.root == node;
    }

    @Override
    public Iterator<Position<E>> iterator() {
        return new InOrderBinaryTreeIterator<>(this);
    }

    @Override
    public Position<E> left(Position<E> p) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (!hasLeft(p))
            throw new RuntimeException("No left child");
        return node.getLeft();
    }

    @Override
    public Position<E> right(Position<E> p) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (!hasRight(p))
            throw new RuntimeException("No right child");
        return node.getRight();
    }

    @Override
    public boolean hasLeft(Position<E> p) {
        BTNode<E> node = checkPosition(p);
        return node.getLeft() != null;
    }

    @Override
    public boolean hasRight(Position<E> p) {
        BTNode<E> node = checkPosition(p);
        return node.getRight() != null;
    }

    @Override
    public E replace(Position<E> p, E e) {
        BTNode<E> node = checkPosition(p);
        E element = node.getElement();
        node.setElement(e);
        return element;
    }

    @Override
    public Position<E> sibling(Position<E> p) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (node == root)
            throw new RuntimeException("No sibling");
        BTNode<E> parent = node.getParent();
        BTNode<E> sibiling;
        if (parent.getLeft() == node){
            if (parent.getRight() == null)
                throw new RuntimeException("No sibling");
            else
                sibiling = parent.getRight();
        } else {
            if (parent.getLeft() == null)
                throw new RuntimeException("No sibling");
            else
                sibiling = parent.getLeft();
        }
        return sibiling;
    }

    @Override
    public Position<E> insertLeft(Position<E> p, E e) throws RuntimeException {
        if (hasLeft(p))
            throw new RuntimeException("Node already has a left child");
        BTNode<E> node = checkPosition(p);
        BTNode<E> leftChild = new BTNode<>(e,node,null,null, this);
        node.setLeft(leftChild);
        return leftChild;
    }

    @Override
    public Position<E> insertRight(Position<E> p, E e) {
        if (this.hasRight(p))
            throw new RuntimeException("Node already has a right child");
        else {
            BTNode<E> node = checkPosition(p);
            BTNode<E> right = new BTNode<>(e, node, null, null, this);
            node.right = right;
            return right;
        }
    }

    @Override
    public E remove(Position<E> p) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        BTNode<E> leftPos = node.getLeft();
        BTNode<E> rightPos = node.getRight();
        if (leftPos != null && rightPos != null) {
            throw new RuntimeException("Cannot remove node with two children");
        }
        //the only child of v, if any, null otherwise
        BTNode<E> child = leftPos != null ? leftPos : rightPos;

        if (node == root) { // v is the root
            if (child != null) {
                child.setParent(null);
            }
            root = child;
        } else { // v is not the root
            BTNode<E> parent = node.getParent();
            if (node == parent.getLeft()) {
                parent.setLeft(child);
            } else {
                parent.setRight(child);
            }
            if (child != null) {
                child.setParent(parent);
            }
        }
        return p.getElement();
    }

    @Override
    public void swap(Position<E> p1, Position<E> p2) {
        BTNode<E> node1 = checkPosition(p1);
        BTNode<E> node2 = checkPosition(p2);

        BTNode<E> copyNode1 = new BTNode<>(node1.element, node1.parent, node1.left, node1.right, this);

        node1.parent = node2.parent == node1 ? node2 : node2.parent;
        node1.left = node2.left == node1 ? node2 : node2.left;
        node1.right = node2.right == node1 ? node2 : node2.right;

        node2.parent = copyNode1.parent == node2 ? node1 : copyNode1.parent;
        node2.left = copyNode1.left == node2 ? node1 : copyNode1.left;
        node2.right = copyNode1.right == node2 ? node1 : copyNode1.right;

        if (node1.parent != null) {
            if (node1.parent.left == node2) {
                node1.parent.left = node1;
            } else {
                node1.parent.right = node1;
            }
        } else {
            this.root = node1;
        }

        if (node2.parent != null) {
            if (node2.parent.left == node1) {
                node2.parent.left = node2;
            } else {
                node2.parent.right = node2;
            }
        } else {
            root = node2;
        }

        if (this.hasLeft(node1)) {
            node1.left.parent = node1;
        }
        if (this.hasRight(node1)) {
            node1.right.parent = node1;
        }
        if (this.hasLeft(node2)) {
            node2.left.parent = node2;
        }
        if (this.hasRight(node2)) {
            node2.right.parent = node2;
        }
    }

    @Override
    public BinaryTree<E> subTree(Position<E> v) {
        BTNode<E> newRoot = checkPosition(v);

        if (newRoot == root)
            root = null;
        else {
            if (newRoot.parent.left == newRoot)
                newRoot.parent.left = null;
            else
                newRoot.parent.right = null;
        }

        newRoot.parent = null;

        LinkedBinaryTree<E> tree = new LinkedBinaryTree<>();
        tree.root = newRoot;
        return tree;
    }

    @Override
    public void attachLeft(Position<E> p, BinaryTree<E> tree) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (tree == this) {
            throw new RuntimeException("Cannot attach a tree over himself");
        }
        if (this.hasLeft(p)) {
            throw new RuntimeException("Node already has a left child");
        }


        if (tree != null && !tree.isEmpty()) {
            //Check position will fail if tree is not an instance of LinkedBinaryTree
            BTNode<E> r = checkPosition(tree.root());
            node.setLeft(r);
            r.setParent(node);

            //The source tree will be left empty
            LinkedBinaryTree<E> lbt = (LinkedBinaryTree<E>) tree; //safe cast, checkPosition would fail first
            lbt.root = null;
        }
    }

    @Override
    public void attachRight(Position<E> p, BinaryTree<E> tree) throws RuntimeException {
        BTNode<E> node = checkPosition(p);
        if (tree == this) {
            throw new RuntimeException("Cannot attach a tree over himself");
        }
        if (this.hasRight(p)) {
            throw new RuntimeException("Node already has a right child");
        }

        if (tree != null && !tree.isEmpty()) {
            //Check position will fail if tree is not an instance of LinkedBinaryTree
            BTNode<E> r = checkPosition(tree.root());
            node.setRight(r);
            r.setParent(node);

            //The source tree will be left empty
            LinkedBinaryTree<E> lbt = (LinkedBinaryTree<E>) tree; //safe cast, checkPosition would fail first
            lbt.root = null;
        }
    }

    /**
     * Practica 3, ejercicio 1
     */
    @Override
    public boolean isComplete() {
        InOrderBinaryTreeIterator<E> it = new InOrderBinaryTreeIterator<>(this);
        while (it.hasNext()){
            Position<E> next = it.next();
            if (isInternal(next))
                if (!hasLeft(next) || !hasRight(next))
                    return false;

        }
        return true;
    }

    private BTNode<E> checkPosition(Position<E> p) {
        if (!(p instanceof BTNode))
            throw new RuntimeException("The position is not valid");

        return(BTNode<E>) p;
    }

}
