package Alex.Quesada.EDA.tree.binaryTree;

import Alex.Quesada.EDA.Position;
import Alex.Quesada.EDA.tree.iterators.InOrderBinaryTreeIterator;

import java.util.Iterator;
import java.util.LinkedList;

public class ArrayBinaryTree<E> implements BinaryTree<E> {

    private class BTPos<T> implements Position<T> {

        private T element;
        private int rank;

        public BTPos(T element, int rank) {
            this.element = element;
            this.rank = rank;
        }

        @Override
        public T getElement() {
            return element;
        }

        public void setElement(T element) {
            this.element = element;
        }

        public int getRank() {
            return rank;
        }

        public void setRank(int rank) {
            this.rank = rank;
        }
    }

    private BTPos<E>[] nodes;
    private int size, capacity;

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Position<E> root() throws RuntimeException {
        if (isEmpty())
            throw new RuntimeException("The tree is empty");
        return nodes[0];
    }

    @Override
    public Position<E> parent(Position<E> v) throws RuntimeException {
        if (v == root())
            throw new RuntimeException("No parent");
        BTPos<E> aux = checkPosition(v);
        int rankParent = aux.getRank() / 2;
        return nodes[rankParent-1];
    }

    @Override
    public Iterable<? extends Position<E>> children(Position<E> v) {
        LinkedList<Position<E>> children  = new LinkedList<>();
        BTPos<E> aux = checkPosition(v);
        int rankLeft = aux.getRank()*2;
        int rankRight = rankLeft + 1;
        if (rankRight <= capacity) {
            if (nodes[rankLeft - 1] != null)
                children.add(nodes[rankLeft - 1]);
            if (nodes[rankRight - 1] != null)
                children.add(nodes[rankRight - 1]);
        }
        return children;
    }

    @Override
    public boolean isInternal(Position<E> v) {
        return !isLeaf(v);
    }

    @Override
    public boolean isLeaf(Position<E> v){
        BTPos<E> aux = checkPosition(v);
        int rankLeft = aux.getRank()*2;
        int rankRight = rankLeft + 1;
        if (rankRight <= capacity)
            return (nodes[rankLeft-1] == null) && (nodes[rankRight-1] == null);
        return true;
    }

    @Override
    public boolean isRoot(Position<E> v) {
        BTPos<E> aux = checkPosition(v);
        return aux == nodes[0];
    }

    @Override
    public Position<E> addRoot(E e) throws RuntimeException {
        if (!isEmpty())
            throw new RuntimeException("Tree already has a root");

        BTPos<E> node = new BTPos<>(e, 1);
        size = 1;
        capacity = 1;
        nodes = new BTPos[capacity];
        nodes[0] = node;
        return node;
    }

    @Override
    public Iterator<Position<E>> iterator() {
        return new InOrderBinaryTreeIterator<>(this);
    }

    @Override
    public Position<E> left(Position<E> v) throws RuntimeException {
        if (!hasLeft(v))
            throw new RuntimeException("No left child");
        BTPos<E> aux = checkPosition(v);
        int rankLeft = 2*aux.getRank();
        return nodes[rankLeft-1];
    }

    @Override
    public Position<E> right(Position<E> v) throws RuntimeException {
        if (!hasRight(v))
            throw new RuntimeException("No right child");
        BTPos<E> aux = checkPosition(v);
        int rankRight = 2 * aux.getRank() + 1;
        return nodes[rankRight-1];
    }

    @Override
    public boolean hasLeft(Position<E> v) {
        BTPos<E> aux = checkPosition(v);
        if (aux.rank * 2 <= capacity){
            return nodes[aux.rank*2-1] != null;
        }
        return false;
    }

    @Override
    public boolean hasRight(Position<E> v) {
        BTPos<E> aux = checkPosition(v);
        if ((aux.rank * 2) + 1 <= capacity){
            return nodes[(aux.rank*2)] != null;
        }
        return false;
    }

    @Override
    public E replace(Position<E> v, E e) {
        BTPos<E> aux = checkPosition(v);
        E element = aux.getElement();
        aux.setElement(e);
        return element;
    }

    @Override
    public Position<E> sibling(Position<E> v) throws RuntimeException {
        BTPos<E> aux = checkPosition(v);
        if (aux == nodes[0])
            throw new RuntimeException("No sibling");
        if (aux.getRank() % 2 == 0) { //v is left sibiling
            if (nodes[aux.getRank()] != null)
                return nodes[aux.getRank()]; // right sibiling
            else
                throw new RuntimeException("No sibling");
        } else {
            if (nodes[aux.getRank() - 2] != null)
                return nodes[aux.getRank() -2]; // right sibiling
            else
                throw new RuntimeException("No sibling");
        }
    }

    @Override
    public Position<E> insertLeft(Position<E> v, E e) throws RuntimeException {
        if (hasLeft(v))
            throw new RuntimeException("Node already has a left child");
        BTPos<E> aux = checkPosition(v);
        int rankLeft = aux.getRank()*2;
        BTPos<E> left = new BTPos<>(e, rankLeft);
        if (capacity < rankLeft)//there's space in the array for the left child
            resizeNodes();
        nodes[rankLeft-1] = left;
        size += 1;
        return left;
    }

    @Override
    public Position<E> insertRight(Position<E> v, E e) throws RuntimeException {
        if (hasRight(v))
            throw new RuntimeException("Node already has a right child");
        BTPos<E> aux = checkPosition(v);
        int rankRight = aux.getRank()*2+1;
        BTPos<E> right = new BTPos<>(e, rankRight);
        if (capacity < rankRight) //there's not enough space in the array for the right child
            resizeNodes();
        nodes[rankRight-1] = right;
        size += 1;
        return right;
    }

    @Override
    public E remove(Position<E> v) throws RuntimeException {
        if (hasLeft(v) && hasRight(v))
            throw new RuntimeException("Cannot remove node with two children");

        BTPos<E> aux = checkPosition(v);
        E element = aux.getElement();
        size--;
        if (!hasLeft(v) && !hasRight(v)) { //no children
            nodes[aux.getRank()-1] = null;
        } else { // 1 child
            int rank = aux.getRank();
            if (hasLeft(v)) {
                auxRemove(rank, 2 * rank);
            } else {
                auxRemove(rank, 2 * rank + 1);
            }
        }
        return element;
    }

    @Override
    public void swap(Position<E> v1, Position<E> v2) {
        BTPos<E> aux1 = checkPosition(v1);
        BTPos<E> aux2 = checkPosition(v2);

        int rankAux1 = aux1.getRank();
        int rankAux2 = aux2.getRank();

        nodes[rankAux1-1] = aux2;
        aux2.setRank(rankAux1);
        nodes[rankAux2-1] = aux1;
        aux1.setRank(rankAux2);
    }

    @Override
    public BinaryTree<E> subTree(Position<E> v) {
        BTPos<E> aux = checkPosition(v);
        BinaryTree<E> newTree = new ArrayBinaryTree<>();
        int rankAux = aux.getRank();
        Position<E> newRoot = newTree.addRoot(aux.getElement());
        auxSubTree(rankAux, newTree, newRoot);
        return newTree;
    }

    @Override
    public void attachLeft(Position<E> v, BinaryTree<E> tree) throws RuntimeException {
        if (hasLeft(v))
            throw new RuntimeException("Node has already a left child");
        if (tree.isEmpty())
            throw new RuntimeException("The parameter tree is empty");
        if (isEmpty())
            throw new RuntimeException("This tree is empty");

        Position<E> child = insertLeft(v, tree.root().getElement());
        if (tree.hasLeft(tree.root())){
            auxAttach(child, tree.left(tree.root()), tree,true);
        }
        if (tree.hasRight(tree.root())){
            auxAttach(child, tree.right(tree.root()), tree,false);
        }
    }

    @Override
    public void attachRight(Position<E> v, BinaryTree<E> tree) throws RuntimeException {
        if (hasRight(v))
            throw new RuntimeException("Node has already a right child");
        if (tree.isEmpty())
            throw new RuntimeException("The parameter tree is empty");
        if (isEmpty())
            throw new RuntimeException("This tree is empty");

        Position<E> child = insertRight(v, tree.root().getElement());
        if (tree.hasLeft(tree.root())){
            auxAttach(child, tree.left(tree.root()), tree,true);
        }
        if (tree.hasRight(tree.root())){
            auxAttach(child, tree.right(tree.root()), tree,false);
        }
    }



    @Override
    public boolean isComplete() {
        Position<E> r = root();
        return subtreeComplete(r);
    }

    private boolean subtreeComplete(Position<E> node){
        if (isLeaf(node))
            return true;
        else if (!hasRight(node) || !hasLeft(node))
            return false;
        else {
            return subtreeComplete(left(node)) && subtreeComplete(right(node));
        }
    }

    private BTPos<E> checkPosition(Position<E> p){
        if (!(p instanceof BTPos))
            throw new RuntimeException("The position is not valid");
        return (BTPos<E>) p;
    }

    private void resizeNodes(){
        int newCapacity = capacity * 2 + 1;
        BTPos<E>[] newNodes = new BTPos[newCapacity];
        for (int i = 0; i<capacity; i++){
            if (nodes[i] != null)
                newNodes[i] = nodes[i];
        }
        nodes = newNodes;
        capacity = newCapacity;
    }

    private void auxRemove(int newRank, int oldRank){
        nodes[newRank-1] = nodes[oldRank-1];
        nodes[newRank-1].setRank(newRank);
        nodes[oldRank-1] = null;
        size --;
        if (capacity >= oldRank*2){
            if (oldRank % 2 == 0){
                if (nodes[oldRank*2-1] != null){
                    auxRemove(newRank*2, oldRank*2);
                }
                if (nodes[oldRank*2] != null){
                    auxRemove(newRank*2+1, oldRank*2+1);
                }
            } else {
                if (nodes[oldRank*2-1] != null){
                    auxRemove(newRank*2, oldRank*2);
                }
                if (nodes[oldRank*2] != null) {
                    auxRemove(newRank*2+1, oldRank * 2 + 1);
                }
            }
        }
    }

    private void auxSubTree(int rank, BinaryTree tree, Position<E> position){
        size --;
        nodes[rank-1] = null;
        if (capacity >= rank*2){
            if (nodes[rank*2-1] != null){
                Position<E> auxPosition = tree.insertLeft(position, nodes[rank*2-1].getElement());
                auxSubTree(rank*2,tree, auxPosition);
            }
            if (nodes[rank*2] != null){
                Position<E> auxPosition = tree.insertRight(position, nodes[rank*2].getElement());
                auxSubTree(rank*2+1,tree, auxPosition);
            }
        }
    }

    private void auxAttach(Position<E> node, Position<E> auxTreeNode, BinaryTree<E> tree, boolean left){
        Position<E> aux;
        size ++;
        if (left)
            aux = insertLeft(node, auxTreeNode.getElement());
        else
            aux = insertRight(node, auxTreeNode.getElement());

        if (tree.hasLeft(auxTreeNode))
            auxAttach(aux, tree.left(tree.root()), tree,true);

        if (tree.hasRight(auxTreeNode))
            auxAttach(aux, tree.right(tree.root()), tree,false);
    }
}
