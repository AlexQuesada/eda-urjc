package Alex.Quesada.EDA.tree.naryTree;

import Alex.Quesada.EDA.tree.iterators.BFSIterator;
import Alex.Quesada.EDA.Position;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public class LCRSTree<E> implements NAryTree<E> {

    private class Node<T> implements Position<T>{

        private T element;
        private Node<T> parent;
        private Node<T> left; //first child
        private Node<T> right; //first sibiling
        private LCRSTree<T> myTree;

        public Node(T element, Node<T> parent, LCRSTree<T> tree) {
            this.element = element;
            this.parent = parent;
            myTree = tree;
            left = null;
            right = null;
        }

        public Node(T element, Node<T> parent, Node<T> left, Node<T> right, LCRSTree<T> myTree) {
            this.element = element;
            this.parent = parent;
            this.left = left;
            this.right = right;
            this.myTree = myTree;
        }

        public void setElement(T element) {
            this.element = element;
        }

        public Node<T> getParent() {
            return parent;
        }

        public void setParent(Node<T> parent) {
            this.parent = parent;
        }

        public Node<T> getLeft() {
            return left;
        }

        public void setLeft(Node<T> left) {
            this.left = left;
        }

        public Node<T> getRight() {
            return right;
        }

        public void setRight(Node<T> right) {
            this.right = right;
        }

        public LCRSTree<T> getMyTree() {
            return myTree;
        }

        public void setMyTree(LCRSTree<T> myTree) {
            this.myTree = myTree;
        }

        @Override
        public T getElement() {
            return this.element;
        }
    }

    private int size;
    private Node<E> root;

    public LCRSTree() {
        root = null;
        size = 0;
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public boolean isEmpty() {
        return size == 0;
    }

    @Override
    public Position<E> root() throws RuntimeException {
        if (root == null)
            throw new RuntimeException("The tree is empty");
        return root;
    }

    @Override
    public Position<E> parent(Position<E> v) throws RuntimeException {
        Position<E> aux = checkPosition(v).getParent();
        if (aux == null)
            throw new RuntimeException("The node has not parent");
        return aux;
    }

    @Override
    public Iterable<? extends Position<E>> children(Position<E> v) {
        Node<E> node = checkPosition(v);
        List<Position<E>> list = new LinkedList<>();
        Node<E> aux = node.getLeft();
        while (aux != null){
            list.add(aux);
            aux = aux.getRight();
        }
        return list;
    }

    @Override
    public boolean isInternal(Position<E> v) {
        return !isLeaf(v);
    }

    @Override
    public boolean isLeaf(Position<E> v) throws RuntimeException {
        Node<E> node = checkPosition(v);
        return node.getLeft() == null;
    }

    @Override
    public boolean isRoot(Position<E> v) {
        return v == root;
    }

    @Override
    public Position<E> addRoot(E e) throws RuntimeException {
        if (root != null)
            throw new RuntimeException("Tree already has a root");

        Node<E> node = new Node<>(e,null, null, null, this);
        root = node;
        size ++;
        return node;
    }

    @Override
    public Iterator<Position<E>> iterator() {
        return new BFSIterator<>(this);
    }

    @Override
    public E replace(Position<E> p, E e) {
        Node<E> node = checkPosition(p);
        E aux = node.getElement();
        node.setElement(e);
        return aux;
    }

    @Override
    public void swapElements(Position<E> p1, Position<E> p2) {
        Node<E> node1 = checkPosition(p1);
        Node<E> node2 =  checkPosition(p2);
        E aux1 = node1.getElement();
        E aux2 = node2.getElement();
        node1.setElement(aux2);
        node2.setElement(aux1);
    }

    @Override
    public Position<E> add(E element, Position<E> p) {
        Node<E> node = checkPosition(p);
        Node<E> newChild = new Node<>(element, node, null, null, this);
        Node<E> aux = node.getLeft();

        if (aux == null) // si p era una hoja (no tenia hijos)
            node.setLeft(newChild);
        else{
            while(aux.getRight() != null)
                aux = aux.getRight();
            aux.setRight(newChild);
        }

        size ++;
        return newChild;
    }

    @Override
    public void remove(Position<E> p) {
        Node<E> node =  checkPosition(p);
        if (root == node){
            BFSIterator<E> it = new BFSIterator<>(this);
            while (it.hasNext()){
                Node<E> aux = (Node<E>) it.next();
                aux.setMyTree(null);
            }
            root = null;
            size = 0;
        } else {
            //ACTUALIZAR PUNTEROS
            Node<E> parent = node.getParent();
            node.setParent(null);
            if (node == parent.getLeft()){ //si es el primer hijo
                if(node.getRight() == null) //si es hijo unico
                    parent.setLeft(null);
                else //si no es hijo unico
                    parent.setLeft(node.getRight());
            } else { //si no es el primer hijo
                LinkedList<Node<E>> children = (LinkedList<Node<E>>) children(parent);
                for (Node<E> aux: children){
                    if (aux.getRight() == node) {
                        if (node.getRight() == null) //si es el ultimo hijo
                            aux.setRight(null);
                        else // si es un hijo "mediano"
                            aux.setRight(node.getRight());

                    }
                }
            }
            //BORRAR NODOS
            BFSIterator<E> it = new BFSIterator<>(this, p);
            int count = 0;
            while (it.hasNext()){
                Node<E> aux = (Node<E>) it.next();
                aux.setMyTree(null);
                count ++;
            }
            size -= count;
        }
    }

    @Override
    public void moveSubtree(Position<E> pOrig, Position<E> pDest) throws RuntimeException {
        //recorrer los nodos de pOrg para comprobar si pDest es un subnodo de pOrg
        Node<E> node1 = checkPosition(pOrig);
        Node<E> node2 = checkPosition(pDest);

        if (pOrig == root)
            throw new RuntimeException("Root node can't be moved");
        if (pOrig == pDest) {
            throw new RuntimeException("Both positions are the same");
        }
        BFSIterator<E> it = new BFSIterator<>(this, pOrig);
        while (it.hasNext()) {
            if (it.next() == pDest)
                throw new RuntimeException("Target position can't be a sub tree of origin");
        }
        //Desasociar node1
        Node<E> parent = node1.getParent();
        if (node1 == parent.getLeft()){ //si node1 es el primer hijo
            if(node1.getRight() == null) {//si node1 es hijo unico
                parent.setLeft(null);
            } else {//si node1 no es hijo unico
                parent.setLeft(node1.getRight());
                node1.setRight(null);
            }
        } else { //si node1 no es el primer hijo
            LinkedList<Node<E>> children = (LinkedList<Node<E>>) children(parent);
            for (Node<E> aux: children){
                if (aux.getRight() == node1) {
                    if (node1.getRight() == null) //si node1 es el ultimo hijo
                        aux.setRight(null);
                    else { // si node1 es un hijo "mediano"
                        aux.setRight(node1.getRight());
                        node1.setRight(null);
                    }
                }
            }
        }

        //reasociar node1
        node1.setParent(node2);
        if (node2.getLeft() == null)
            node2.setLeft(node1);
        else{
            LinkedList<Node<E>> children = (LinkedList<Node<E>>) children(node2);
            for (Node<E> aux: children){
                if (aux.getRight() == null)
                    aux.setRight(node1);
            }
        }
    }

    private Node<E> checkPosition(Position<E> p) throws RuntimeException{

        if (!(p instanceof Node))
            throw new RuntimeException("The position is no valid");

        Node<E> node = (Node<E>) p;
        if (node.getMyTree() != this)
            throw new RuntimeException("The node is not from this tree");

        return node;
    }
}