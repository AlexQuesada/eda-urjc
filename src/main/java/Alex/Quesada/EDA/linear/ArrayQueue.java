package Alex.Quesada.EDA.linear;

public class ArrayQueue<E> implements Queue<E> {

    private E[] elements;
    private int size, head, tail, capacity;
    private static final int INIT_CAPACITY = 2;

    public ArrayQueue() {
        this(INIT_CAPACITY);
    }

    public ArrayQueue(int capacity) {
        this.elements = (E[]) new Object[capacity];
        this.size = 0;
        this.head = 0;
        this.tail = 0;
        this.capacity = capacity;
    }

    @Override
    public int size() {
        return this.size;
    }

    @Override
    public boolean isEmpty() {
        return this.size == 0;
    }

    @Override
    public E front() throws RuntimeException{
        if (isEmpty())
            throw new RuntimeException("Queue is empty");
        return elements[head];
    }

    @Override
    public void enqueue(E element) {
        if (size == capacity){ //si hay que redimensionar el array
            int newCapacity =  capacity * 2;
            E[] newElements = (E[]) new Object[newCapacity];
            for (int i = 0; i < capacity; i++)
                newElements[i] = (head + i < capacity) ? elements[head + i] : elements[i-(capacity-head)];
            head = 0;
            tail = capacity - 1;
            elements = newElements;
            capacity = newCapacity;
        }

        elements[tail] = element;
        size++;
        tail = (tail == capacity - 1) ? tail = 0 : tail ++;
    }

    @Override
    public E dequeue() throws RuntimeException{
        if (this.isEmpty())
            throw new RuntimeException("Queue is empty");
        size --;
        E element = elements[head];
        head = (head == capacity -1) ? head = 0 : head ++;
        return element;
    }
}